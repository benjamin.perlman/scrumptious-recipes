from django.contrib import admin


from meal_plan.models import MealPlan


class MealPlanAdmin(admin.ModelAdmin):
    pass


admin.site.register(MealPlan, MealPlanAdmin)
