from django.urls import path
from meal_plan.views import (
    MealPlanListView,
    MealPlanCreateView,
    MealPlanDeleteView,
    MealPlanUpdateView,
    MealPlanDetailView,
)


urlpatterns = [
    #  path("meal_plan/", include("meal_plan.urls")),
    path("", MealPlanListView.as_view(), name="meal_plan_list"),
    path("<int:pk>/", MealPlanDetailView.as_view(), name="meal_plan_detail"),
    path(
        "<int:pk>/delete/",
        MealPlanDeleteView.as_view(),
        name="meal_plan_delete",
    ),
    path("new/", MealPlanCreateView.as_view(), name="meal_plan_new"),
    path("<int:pk>/edit/", MealPlanUpdateView.as_view(), name="meal_plan_edit"),
    # path("<int:meal_plan_id>/ratings/", log_rating, name="meal_plan_rating"),
]
