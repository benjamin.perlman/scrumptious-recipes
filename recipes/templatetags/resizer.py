from django import template

register = template.Library()


def resize_to(ingredient, target):
    # Get the number of servings from the ingredient's
    # recipe using the ingredient.recipe.servings
    # properties

    # If the servings from the recipe is not None
    #   and the value of target is not None
    # try
    # calculate the ratio of target over
    #   servings
    # return the ratio multiplied by the
    #   ingredient's amount
    # catch a possible error
    # pass
    # return the original ingredient's amount since
    #   nothing else worked

    num = ingredient.recipe.servings
    num2 = ingredient.amount
    # = int(self.request.GET.get("servings"))
    if (num is not None) and (target is not None):
        #   try:
        ratio = int(target) / num
        print(num)
        print(target)
        print(num2)
        return ratio * int(num2)
    # except num.DoesNotExist:
    #      pass
    return ingredient


register.filter(resize_to)
